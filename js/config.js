/**
 * Created by ds on 12.03.15.
 */

require([], function () {
    require.config({
        deps: ['material_apps/app'],
        baseUrl: '/',
        urlArgs: 'v=' + Drupal.settings.cache_key,
        packages: Drupal.settings.material_apps_requirejs.packages || {},
        optimize : "uglify2",
        paths: Drupal.settings.material_apps_requirejs.paths || {},
        shim: Drupal.settings.material_apps_requirejs.shim || {}
    });

});
