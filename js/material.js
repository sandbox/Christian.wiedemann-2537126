define(['angular', 'angularAMD', 'ngMaterial', 'ngAria', 'angular-growl', 'ngSanitize'], function (angular, angularAMD) {
    var app = angular.module("material", ['ngMaterial', 'angular-growl', 'ngSanitize']);
    app.factory('httpPreConfig', ['$http', '$rootScope', function($http, $rootScope) {
        $http.defaults.transformRequest.push(function (data) {
            $rootScope.$broadcast('httpCallStarted');
            return data;
        });
        $http.defaults.transformResponse.push(function(data){
            $rootScope.$broadcast('httpCallStopped');
            return data;
        })
        return $http;
    }]);


    app.config(['growlProvider', function (growlProvider) {
        growlProvider.globalEnableHtml(true);
        growlProvider.globalTimeToLive(5000);
        growlProvider.onlyUniqueMessages(false);
    }]);
    app.controller('ListEntityController', function ($scope, $http) {


    });

    app.controller('MaterialViewController', function ($scope, $element, AppState) {
        var domId;
        $scope.refreshView = function () {
            jQuery('.views-exposed-form .form-submit', $element).click();
        }
        jQuery('h2.md-view-title', $element).css('color', '#' + Drupal.settings.materialTextColor);

        var $exposed = jQuery($element).find('.views-exposed-widgets');

        jQuery.grep($element.attr('class').split(' '), function (v, i) {
            if (v.indexOf('view-dom-id-') === 0) {
                domId = v;
            }
        })


        $scope.handleState=function(){
            var state = AppState.getViewState(domId);
            if (state != 'open' ) {
                $exposed.hide();
                jQuery($element).find('.filter-arrow-up').hide();
                jQuery($element).find('.filter-arrow-down').show();
            } else {
                $exposed.show();
                jQuery($element).find('.filter-arrow-down').hide();
                jQuery($element).find('.filter-arrow-up').show();
            }
        }
        $scope.handleState();
        $scope.toggle = function (event) {

            $exposed.toggle();
            if ($exposed.is(':visible')) {
                AppState.setViewState(domId, 'open');
            } else {
                AppState.setViewState(domId, 'closed');
            }
            $scope.handleState();
            event.preventDefault()
            return false;
        }


        jQuery('select[name="sort_by"]', $element).change(function () {
            $scope.refreshView();
        })

    })

    app.controller('ToastController', function ($scope, growl, $timeout, $element) {
        $timeout(function(){
            jQuery('.message', $element).each (function (){
                var type = jQuery(this).attr('data-type');
                var text = jQuery(this).html();
                switch (type) {
                    case 'status':
                        growl.addSuccessMessage(text, {ttl: 3000});
                        break;
                    case 'error':
                        growl.addErrorMessage(text);
                        break;
                    case 'warning':
                        growl.addWarnMessage(text);
                        break;
                    default:
                        growl.addInfoMessage(text);
                        break;

                }
                jQuery(this).remove();
            })
        })



    });
    app.controller('TabsController', function ($scope, $window, $location, AppState, $rootScope) {
        //$scope.tabs = Drupal.settings.material_apps.tabs;
        $scope.tabs = [{title: 'dummy'}];
        $scope.hash = '';
        $scope.selectedIndex = 0;
        $scope.serverSelectedIndex = 0
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                var pright = AppState.getViewPage('view-right');
                if (pright != null) {
                    if ($scope.hash != pright.tabs.hash) {
                        $scope.tabs = pright.tabs.tabs;
                        $scope.hash = pright.tabs.hash;
                    }
                    $scope.selectedIndex = pright.tabs.selectedIndex;
                    $scope.serverSelectedIndex = $scope.selectedIndex;
                }

            });
        $rootScope.$on('locationChangedAborted', function(event, args) {
            $scope.selectedIndex = $scope.serverSelectedIndex;
        });
        $scope.switchTab = function (tab) {
            if ($scope.tabs.length == 0) {
                return;
            }
            if ($scope.selectedIndex == $scope.serverSelectedIndex) {
                return;
            }
            $window.location.hash = AppState.linkState(tab.path, 3);
        }
    });

    app.controller('GlobalActionButtonController', function ($scope, $rootScope, AppState, $timeout,$mdDialog,$http,$state) {
        $scope.actions = [];
        $scope.click = function (action,ev) {
            if (action.icon=='delete' && action.entity_type!=null)  {
                var confirm = $mdDialog.confirm()
                    .parent(angular.element(document.body))
                    .title('Delete "'+action.entity_label+'"')
                    .content(Drupal.t('Are you sure you want to delete this item? This cannot be undone!'))
                    .ariaLabel(Drupal.t('Delete'))
                    .ok(Drupal.t('Delete'))
                    .cancel(Drupal.t('Cancel'))
                    .targetEvent(ev);
                $mdDialog.show(confirm).then(function() {
                    $http.get('/material/delete/'+action.entity_type+'/'+action.entity_id).
                        success(function(data, status, headers, config) {
                            if ($state.$current.parent!=null){
                                var targetState = $state.get($state.$current.parent.name + '-grid');
                                if (targetState!=null) {
                                    $state.go($state.$current.parent.name + '-grid');
                                }
                            }
                        }).
                        error(function(data, status, headers, config) {
                        });
                }, function() {
                    // Do nothing
                });
                return ;
            }
            jQuery('#'+action.id).mousedown();
        }
        $rootScope.$on('$viewContentLoaded',
            function (event, toState, toParams, fromState, fromParams) {
                var pright = AppState.getViewPage('view-right');
                if (pright != null) {
                    $timeout(function () {
                        $scope.actions = [];
                        jQuery('#view-right-content .md-actions input.asaf-control-asaf_submit').each(function () {
                            //jQuery(this).hide();
                            $scope.actions.push({
                                id: jQuery(this).attr('id'),
                                title: jQuery(this).attr('value'),
                                icon: jQuery(this).attr('icon'),
                                entity_id: jQuery(this).attr('data-entity_id'),
                                entity_type: jQuery(this).attr('data-entity_type'),
                                entity_label: jQuery(this).attr('data-entity_label')
                            })
                        })
                    })
                }
            });
    })

    app.controller('ListHighlightController', function ($scope, $element, $rootScope, $timeout) {



        $scope.highlightItem = function(id, element){
            jQuery('md-list-item', $element).addClass('md-primary');
            jQuery('md-list-item button', element).removeClass('md-theme md-hue-200');
            jQuery('.entity-id-' + id+' button', element).addClass('md-theme md-hue-200');
        }

        $scope.linkEntity = function(id,entity_type,bundle,url){
            $scope.highlightItem(id,$element);
            $scope.$parent.linkEntity(id,entity_type,bundle,url);
        }

        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {

                $timeout(function () {
                    if(toParams.id != null){
                        $scope.highlightItem(toParams.id,$element)
                    }
                })
            });
    })
    app.controller('FabController', function ($scope, $window, $location, AppState, $rootScope, $timeout) {
        //$scope.tabs = Drupal.settings.material_apps.tabs;
        $scope.actions = [];
        $scope.renderSingle = false;
        $scope.renderMulti = false;
        $scope.goAndScroll = function(action){
            $scope.go(action)
            jQuery('md-content').scrollTop(0);
        }
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                $scope.renderSingle = false;
                $scope.renderMulti = false;
                var pright = AppState.getViewPage('view-right');
                var pleft = AppState.getViewPage('view-left');
                var actions = JSON.parse(JSON.stringify(pleft.actions)); //clone
                if (typeof pright != 'undefined') {
                    for (var i in pright.actions) {
                        actions.push(pright.actions[i]);
                    }
                }
                $scope.actions = actions;
                if (actions.length == 1) {
                    $scope.renderSingle = true;
                } else if (actions.length > 1){
                    $scope.renderMulti = true;
                }else{
                    $scope.renderSingle = false;
                    $scope.renderMulti = false;
                }


                //$scope.render = true;

            });
    });

    app.controller('PageController', function ($scope, $mdSidenav, $state, AppState, $mdDialog, $location, $http, $rootScope, $window, $mdMedia, $timeout,httpPreConfig,Storage) {
        var activeAjaxCalls = 0;

        Drupal.ajax.prototype.commands.changeUIRouterState = function(ajax, response){
            $state.go(response.params.state, response.params.params, response.params.options)
        }
        function showAjaxLoader(){
            if(activeAjaxCalls == 0){
                jQuery('#ajax-loader').show();
            }
            activeAjaxCalls++;
        }

        function hideAjaxLoader(){
            activeAjaxCalls--;
            if(activeAjaxCalls == 0){
                jQuery('#ajax-loader').hide();
            }
        }
        jQuery(document).ajaxStart(function () {
            showAjaxLoader();
        })
            .ajaxStop(function () {
                hideAjaxLoader();
            });
        $scope.$on('httpCallStarted', function(e) {
            showAjaxLoader();
        });
        $scope.$on('httpCallStopped', function(e) {
            hideAjaxLoader();
        });
        var app = AppState.getCurrentApp();
        $scope.go = function (path, closeSidebar) {
            $location.path(path);
            if (closeSidebar == true) {
                $mdSidenav('menu').close();
            }
        }

        $scope.fullscreen = false;
        $scope.fullscreenIcon = 'fullscreen'
        $scope.toggleFullscreen = function(){
            jQuery('body').toggleClass('fullscreen')
            $scope.fullscreen = !$scope.fullscreen;
            if($scope.fullscreen){
                $scope.fullscreenIcon = 'fullscreen_exit'
            }else{
                $scope.fullscreenIcon = 'fullscreen'
            }
        }


        $scope.showDirtyConfirm = function(event,formId,newUrl) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title(Drupal.t('There are unsaved changes!'))
                .content(Drupal.t('Are you sure you want to leave this page?'))
                .ok(Drupal.t('Yes'))
                .cancel(Drupal.t('No'))
                .targetEvent(event);
            $mdDialog.show(confirm).then(function() {
                Drupal.settings.form_states[formId]=jQuery('#'+formId).serialize();
                window.location = newUrl;
            },function (){
                $rootScope.$broadcast('locationChangedAborted')

            });
        };
        //$scope.showDirtyConfirm();
        $scope.$on('$locationChangeStart', function( event ,newUrl,oldUrl) {
            for (var form_state in Drupal.settings.form_states) {
                var oFormState = Drupal.settings.form_states[form_state];
                if (jQuery('#'+form_state).length!=0) {
                    var nFormState = jQuery('#'+form_state).serialize();
                    if (oFormState!=nFormState) {
                        event.preventDefault();
                        $scope.showDirtyConfirm(event,form_state,newUrl);
                    }
                }
            }
            /*var answer = confirm("Are you sure you want to leave this page?")
            if (!answer) {
                event.preventDefault();
            }*/
        });
        $scope.overlay = function(url, event){

            var sep = url.indexOf('/') === 0?'':'/';
            Storage.loadData('/material/get'+sep+url).then(function(response){
                var DialogController = function ($scope, $mdDialog) {
                    $scope.linkEntity = function (id,entity_type,bundle,url) {
                        $mdDialog.hide();
                        if (url!=null){
                            window.location.href=url;
                        }
                        return false;
                    }
                    $scope.closeDialog = function() {
                        $mdDialog.hide();
                    }
                }
                $mdDialog.show({
                    controller: DialogController,
                    template:'<md-dialog style="width:610px"><md-dialog-content>'+response.content+'</md-dialog-content><div class="md-actions" style="background-color:#f5f5f5;border-top:1px solid #ccc"><md-button ng-click="closeDialog()" class="md-primary">Close</md-button></div></md-dialog>'
                })
            })
        }


        $scope.hasMenu = false;
        if (app != null) {
            $scope.hasMenu = app.menu.length > 0;
        }

        $scope.setPageState = function (pageState) {
            AppState.setCurrentPageState(pageState);
        }
        $scope.linkEntity = function (id,entity_type,bundle,url) {
            if (url!=null){
                if (AppState.getCurrentPageState()=='left'){
                    AppState.setCurrentPageState('right');
                }
                window.location.href=url;
            }
            return false;
        }
        $scope.linkState = function (url, partCount) {
            $window.location.hash = AppState.linkState(url, partCount);
            return false;
        }
        $scope.hasView = function (current,view_name) {
            if (current.views[view_name] != null) {
                return true;
            } else if (current.parent != null) {
                return $scope.hasView(current.parent,view_name);
            } else {
                return false;
            }
        }
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                var pageState = null;
                if (fromParams.action != null) {
                    jQuery('#view-right').removeClass('action-' + fromParams.action);
                }
                if (toParams.action != null) {
                    jQuery('#view-right').addClass('action-' + toParams.action);
                }
                if ($scope.hasView($state.$current,'view-right@')) {
                    jQuery('body').addClass('has-right-view');
                    pageState = 'right';
                } else {
                    jQuery('body').removeClass('has-right-view');
                    pageState = 'full';
                }
                if ($scope.hasView($state.$current,'view-left@')) {
                    jQuery('body').addClass('has-left-view');
                } else {
                    jQuery('body').removeClass('has-left-view');
                    pageState = 'right';
                }
                AppState.setCurrentPageState(pageState);
                var pright = AppState.getViewPage('view-right');
                if (pright != null) {
                    $scope.title_right = pright.title;
                }

            });


        $scope.$watch(AppState.getCurrentPageState, function (state) {
            $scope.pageState = state;
            jQuery('body').attr('data-page-state', state);
        })
        $scope.toggleNav = function () {
            $mdSidenav('menu').toggle()
        }
        $scope.togglePopUp = function (selector) {
            jQuery(selector).fadeToggle();
        }
    })

    app.controller('OpenInOverlayController', function ($scope) {
        $scope.linkEntity = function (id,entity_type,bundle,url,internal_url) {
            $scope.overlay(internal_url);
            return false;
        }

    });
    app.controller('LeftNavigationController', function ($scope, AppState, $mdSidenav) {
        $scope.items = AppState.getCurrentApp() != null ? AppState.getCurrentApp().menu : [];
    });

    app.config(function ($mdThemingProvider, $mdIconProvider) {
        $mdIconProvider.fontSet('socicon', 'socicon');

        var rylPalette = {
            '50': 'FDECDE',
            '100': 'FAD1B0',
            '200': 'F7B57F',
            '300': 'F4984C',
            '400': 'F28226',
            '500': 'EF6C00',
            '600': 'DA6200',
            '700': 'C25800',
            '800': 'AA4C00',
            '900': '7D3800',
            'A100': 'ef6c00',
            'A200': 'ef6c00',
            'A400': 'ef6c00',
            'A700': 'ef6c00',
            'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                                // on this palette should be dark or light
            'contrastDarkColors': undefined,
            'contrastLightColors': undefined    // could also specify this if default was 'dark'
        }



        $mdThemingProvider.definePalette('rylPalette', rylPalette);
        var material_apps = Drupal.settings.material_apps || {};


        var currentPalette = rylPalette;

        if (material_apps.palette != null) {


            $mdThemingProvider.definePalette('currentAppPalette', material_apps.palette);
            $mdThemingProvider.theme('default')
                .primaryPalette('currentAppPalette')
                .accentPalette('rylPalette')

            $mdThemingProvider.theme('junitybase')
                .primaryPalette('rylPalette')
            $mdThemingProvider.theme('light')


            currentPalette = material_apps.palette
            var textColor = material_apps.palette['500'];
            var highlightColor = material_apps.palette['200']
            var hoverColor = material_apps.palette['100']
            var secondaryTextColor = material_apps.palette['800'];
        } else {
            $mdThemingProvider.theme('default')
                .primaryPalette('rylPalette')
                .accentPalette('rylPalette')

            var textColor = rylPalette['500'];
            var highlightColor = rylPalette['200'];
            var hoverColor = rylPalette['100'];
            var secondaryTextColor = rylPalette['800'];
        }


        Drupal.settings.materialTextColor = textColor;
        var customStyles = '';
        customStyles += '.md-primary{color:#' + textColor + '} a{color:#' + textColor + '} .md-secondary{color:#' + secondaryTextColor + '}';
        customStyles += 'a.md-button.md-theme:not([disabled]).md-focused, .md-button.md-theme:not([disabled]).md-focused, md-list md-list-item button.md-theme.md-button:not([disabled]):hover {background-color: #'+highlightColor+'} '
        customStyles += 'a.md-button:not([disabled]).md-focused, .md-button:not([disabled]).md-focused, md-list md-list-item button.md-button:not([disabled]):hover {background-color: #'+hoverColor+'} '

        jQuery.each(currentPalette, function (hue, hex) {
            customStyles += '.md-theme.md-hue-' + hue + '{background-color:#' + hex + '} ';

        })

        jQuery('<style type="text/css">' + customStyles + '</style>').appendTo('head');


    })
});