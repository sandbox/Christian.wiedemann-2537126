var paths = ['angularAMD', 'ngAnimate', 'angular-route', 'material_apps/material', 'angular-ui-router'];
var ngModules = ['ngRoute', 'material', 'ui.router'];
var material_apps = Drupal.settings.material_apps || {};
var modules = material_apps.ngModules || {};


for (var i in modules) {
    var ngModule = modules[i];
    paths.push(ngModule.path);
    ngModules.push(i);
}

Drupal.behaviors.MaterialApp = {

    attach: function (context, options) {
        if (context != document && context != null) {
            jQuery(context).once('angular-compiled', function () {
                jQuery('a', context).once('angular-click-compiled', function () {
                    var href = jQuery(this).attr('href')
                    var pat = /^https?:\/\//i;
                    if (!pat.test(href))
                    {
                        jQuery(this).attr('href', '#' + href);
                    }

                });
            })
        }
    }
}
define(paths, function (angularAMD) {
    var app = angular.module("material_apps", ngModules);
    app.value('materialSettings',
        Drupal.settings.material_apps
    );
    app.factory('Storage', function ($http, $q,AppState) {
        var urls = [];

        return {
            loadData: function (url,viewName,stateName) {
                var deferred = $q.defer();
                viewName = viewName==null?'':viewName;
                viewName = viewName.replace('@', '');
                var stateParam = stateName!=null?'&material-state-name=' + stateName:'';
                var viewParam = viewName!=null?'material-view=' + viewName:'';
                var seperator = url.indexOf('?') == -1 ? '?' : '&';
                url += seperator +  viewParam + stateParam+ '&MATERIAL_APP=' + AppState.getCurrentApp().name;
                console.log('load: ' + ' view ' + viewName + ' url' + url)

                $http({url: url, headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
                }}).then(function (response) {
                        if (typeof response.data === 'object') {
                            var loaded_urls = [];
                            var data = response.data;

                            if (viewName!='') {
                                AppState.setViewPage(viewName, data.page);
                            }
                            var req_urls = data.require_js;
                            var urls = [];
                            for (var url in req_urls) {
                                if (loaded_urls[req_urls[url]] == null) {
                                    loaded_urls[req_urls[url]] = true;
                                    urls.push(req_urls[url]);
                                }
                            }
                            if (urls.length != 0) {
                                require(
                                    urls,
                                    function (app) {
                                        angularAMD.processQueue();
                                        deferred.resolve(data);
                                    }
                                );
                            } else {
                                deferred.resolve(data);
                            }

                        } else {
                            // invalid response
                            return $q.reject(response.data);
                        }
                    }, function (response) {
                        // something went wrong
                        return $q.reject(response.data);
                    });
                return deferred.promise;
            }
        }
    })
    app.directive('ngscript', function ($timeout) {
        return {
            restrict: 'E',
            scope: false,
            link: function (scope, elem, attr) {
                if (elem.parent().attr('id') == 'script-container') {
                    return;
                }
                if (elem.attr('nsrc') != undefined) {

                    var found = false;
                    var src = elem.attr('nsrc');
                    var found = false;
                    jQuery('script').each(function () {
                        var script_src = jQuery(this).attr('src');
                        if (script_src == src) {
                            found = true;
                            return;
                        }
                    });
                    if (!found) {
                        if (!(src.indexOf('requirejs') > -1) && !(src.indexOf('misc/ajax.js') > -1)) {
                            jQuery('#script-container').append('<script async="false" src="' + src + '" type="text/javascript"></script>');
                        }
                    }
                } else if (attr.type == 'text/javascript') {
                    var code = elem.html();
                    jQuery('#script-container').append('<script>' + code + '</script>');
                }
                jQuery(elem).remove()
            }
        };
    });
    app.directive('ngattach', function ($timeout) {
        return {
            restrict: 'E',
            scope: false,
            link: function (scope, elem, attr) {
                var new_content = jQuery(elem).parent().contents();
                $timeout(function(){
                    Drupal.attachBehaviors(jQuery(elem).parent(), Drupal.settings);
                })
                console.log('attachBehaviors')
            }
        };
    });
    getTemplate = function (url, viewName, stateName, urlDecode) {
        var loaded_urls = [];
        return function ($state, $stateParams, $templateCache, $http, Storage, AppState, $q) {
            if (urlDecode == true) {
                var internalUrl = '/material/get' + decodeURI($stateParams.include);
            } else {
                var internalUrl = url;
                for (var param in $stateParams) {
                    internalUrl = internalUrl.replace('[' + param + ']', $stateParams[param])
                }
            }
            return Storage.loadData(internalUrl,viewName,stateName).then(function (data) {
                return data.content;

            });
        }
    }
    app.config(function ($stateProvider, $urlRouterProvider) {
        if (Drupal.settings.material_apps == null) {
            return;
        }

        var default_route = null;
        if (Drupal.settings.material_apps.app.menu[0]!=null){
            default_route = Drupal.settings.material_apps.app.menu[0].route;
        }
        console.log(default_route);
        for (var state_name in Drupal.settings.material_apps.states) {
            var state = Drupal.settings.material_apps.states[state_name];
            if (default_route == null) {
                default_route = state_name;
            }

            var spstate = {};
            spstate.url = state.url;
            spstate.abstract = state.abstract;
            spstate.views = {};
            for (var view_name in state.views) {

                spstate.views[view_name] = {
                    //controller:getController(state.views[view_name]),
                    templateProvider: getTemplate(state.views[view_name].url, view_name, state.base, state.views[view_name].url_decode)
                }
            }
            $stateProvider.state(state_name, spstate);
        }
        if (default_route != null) {
            $urlRouterProvider.otherwise(function ($injector, $location) {
                if ($location.path() == '') {
                    $location.path('/' + default_route);
                } else {
                    var $state = $injector.get('$state');
                    var current = $state.$current;
                    while (current != null) {
                        var incState = $state.get(current.self.name + '.include');
                        if (incState != null) {
                            $state.go(incState.name, {include: encodeURI($location.url())})
                        }
                        current = current.parent;
                    }

                }
            });
        }

    })
    app.controller('InputScopeController', function ($scope, $element, $timeout) {
        $scope.$watch('date', function (oldValue, newValue) {
            if (newValue != null) {
                $scope.value = new Date(newValue)
            }
        });
        $scope.$watch('value', function (oldValue, newValue) {
            if (oldValue != newValue) {
                $timeout(function () {
                    jQuery('input', $element).add('select', $element).trigger('change');
                })
            }
        })
    });
    app.factory('Entity', function (restmod) {
        return function (entity_type) {
            var mod = restmod.model('/' + entity_type, {
                    '~before-request': function (_req) {
                        _req.headers = {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': Drupal.settings.material_apps.token
                        }
                    }
                }
            );
            return mod;
        }
    });
    app.factory('AppState', function (materialSettings, $window) {
        if (materialSettings == null) {
            materialSettings = {};
            materialSettings.user = 0;
        }
        var currentUser = materialSettings.user;
        var currentEntity;
        var currentApp = materialSettings.app;
        var currentPageState;
        var currentViewPage = [];
        var currentViewState = [];
        return {
            linkState: function (url, partCount) {
                var url = url.split('/');
                var lastPart = url[url.length - 1];
                var currentUrlParts = $window.location.hash.split('/');
                var url = '';
                for (var i = 1; i < partCount; i++) {
                    url += '/' + currentUrlParts[i];
                }
                return url + '/' + lastPart;
            },
            getCurrentUser: function () {
                return currentUser;
            },
            setCurrentApp: function (_currentApp) {
                currentApp = _currentApp;
            },
            getCurrentPageState: function () {
                return currentPageState;
            },
            setCurrentPageState: function (_currentPageState) {
                currentPageState = _currentPageState;
            },
            getCurrentApp: function () {
                return currentApp;
            },
            isLoggedIn: function () {
                return currentUser.uid == 0 ? false : true;
            },
            setViewPage: function (view_name, page) {
                currentViewPage[view_name] = page;
            },
            getViewPage: function (view_name) {
                return currentViewPage[view_name];
            },
            setViewState: function (key, state) {
                currentViewState[key] = state;
            },
            getViewState: function (key) {
                return currentViewState[key];
            },
            getViewPages: function () {
                return currentViewPage;
            }


        }
    })
    angularAMD.bootstrap(app, ['material_apps']);
    return app;
});
